package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.Blob;
import play.db.jpa.Model;

/**
 * The Donation class to inherit data and behaviour from the
 * Model class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
@Entity // indicates information to be stored to a database
// this User class represented by a table in the database
public class Donation extends Model {
	// public long received; //superseded by amountDonated
	public long amountDonated;
	public String methodDonated;
	public Date dateDonated;

	@ManyToOne
	public User from; // one user can make many donations

	/**
	 * Establishes the business logic and data required by the Donation model
	 * 
	 * @param from the user the donation submitted by 
	 * @param amountDonated the amount of money in dollars, donated by the user
	 * @param methodDonated the payment method used
	 */
	public Donation(User from, long amountDonated, String methodDonated) {
		this.from = from;
		this.amountDonated = amountDonated;
		this.from = from;
		this.methodDonated = methodDonated;
		dateDonated = new Date();
	}
}