package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.Blob;
import play.db.jpa.Model;

/**
 * The User class to inherit data and behaviour from the Model class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
@Entity // indicates information to be stored to a database
// this User class represented by a table in the database
public class User extends Model {
	public boolean usaCitizen;
	public String firstName;
	public String lastName;

	public String email;
	public String password;

	/**
	 * Establishes the business logic and data required by the User model
	 * 
	 * @param usaCitizen
	 *            whether the user a US citizen or not
	 * @param firstName
	 *            the User first name
	 * @param lastName
	 *            the User last name
	 * @param email
	 *            the user's email
	 * @param password
	 *            the user's login password
	 */
	public User(boolean usaCitizen, String firstName, String lastName, String email, String password) {
		this.usaCitizen = usaCitizen;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	/**
	 * Method to identify the User y reference to thier email address
	 * 
	 * @param email
	 *            the email address actual parameter passed in by the user to
	 *            search by
	 * @return returns the user email in the first instance
	 */
	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	/**
	 * Method to check in the actual parameter email address submitted matches
	 * that of the User email passed in at signup
	 * 
	 * @param password
	 * @return returns true if match exits or false if no match
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
}