package controllers;

import play.*;
import java.util.ArrayList;
import play.mvc.*;

import java.util.*;

import models.*;

/**
 * A Welcome class to inherit data and behaviour from the Controller class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
public class Welcome extends Controller {
	/**
	 * Establishes the home landing page for the site for render to the view
	 */
	public static void index() {
		Logger.info("Landed in Welcome class");
		render();
	}
}