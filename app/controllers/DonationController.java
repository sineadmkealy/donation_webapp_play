package controllers;

import java.util.Date;
import java.util.List;

import models.Donation;
import models.User;
import play.*;
import play.mvc.Controller;

/**
 * The DonationController class to inherit data and behaviour from the
 * Controller class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 */
public class DonationController extends Controller {
	
	/**
	 * Affirms the logged-in User by reference to the Accounts class Establishes
	 * the User donation as a percentage of the donation target (hardcoded in
	 * the getDonationTarget() below.) Renders this User and their Donation
	 * progress in the progress bar in the DonationController view
	 */
	public static void index() {
		User user = Accounts.getCurrentUser();
		if (user == null) {
			Logger.info("Donation class : Unable to getCurrentUser");
			Accounts.login();
		} else {
			String prog = getPercentTargetAchieved();
			String progress = prog + "%";
			// to render the trailing % icon in progress bar view
			Logger.info("Donation controller :  percent target achieved " + progress);
			render(user, progress);
		}
	}

	/**
	 * Method to facilitate User's donation of precoded sums of money from a
	 * pull-down menu in the DonationContoller view.
	 * 
	 * @param amountDonated
	 *            the amount of money, in dollars, donated by user
	 * @param methodDonated
	 *            the payment method used to donate money
	 */
	public static void donate(long amountDonated, String methodDonated, Date dateDonated) {
		Logger.info("amount donated " + amountDonated + " " + "method donated " + methodDonated + "date donated " + dateDonated);

		User user = Accounts.getCurrentUser();// method in Accounts controller
		if (user == null) {
			Logger.info("Donation class : Unable to getCurrentUser");
			Accounts.login();
		} else {
			addDonation(user, amountDonated, methodDonated);
		}
		index(); // to render the user status after donation
	}

	/**
	 * Method to add the user donation to their existing donation balance
	 * 
	 * @param user
	 *            the logged-in user the amount of money, in dollars, donated by
	 *            user
	 * @param amountDonated
	 *            the amount of money, in dollars, donated by user
	 * @param methodDonated
	 *            the payment method used to donate money
	 */
	private static void addDonation(User user, long amountDonated, String methodDonated) {
		Donation bal = new Donation(user, amountDonated, methodDonated);
		bal.save();
	}

	/**
	 * Hardcodes the donation target amount
	 * 
	 * @return The target donation amount
	 */
	private static long getDonationTarget() {
		return 20000;
	}

	/**
	 * Calculates the percentage of funding target achieved by reference to
	 * total donations by all Users for that project. Queries the Donation model
	 * for a list of Donations, calculates the percentage this constitutes of
	 * the donation target and render this percentage to the progress bar.
	 * 
	 * @return the percentage of target achieved to date
	 */
	public static String getPercentTargetAchieved() {
		List<Donation> allDonations = Donation.findAll();
		long total = 0;
		for (Donation donation : allDonations) {
			// total += donation.received;
			total += donation.amountDonated;
		}
		long target = getDonationTarget();
		long percentachieved = (total * 100 / target);
		String progress = String.valueOf(percentachieved);
		return progress;
	}

	/**
	 * Renders a report summary of Donations by all Users Creates an ArrayList
	 * of type Donation to renders this donations list in the renderReport view
	 */
	public static void renderReport() {
		List<Donation> donations = Donation.findAll();
		render(donations);
	}
}