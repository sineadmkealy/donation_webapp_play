
package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

/**
 * An Accounts class to inherit data and behaviour from the Controller class
 * 
 * @author Sinead
 * @version 2.0 June 2016
 * 
 */
public class Accounts extends Controller {
	
	/**
	 * Creates the user signup for rendering in the Accounts signup view
	 */
	public static void signup() {
		render();
	}

	/**
	 * Creates the user signup for rendering in the Accounts login view
	 * 
	 * @param user
	 *            Simulates the logged-in user
	 */
	public static void register(User user) {
		user.save();
		login();
	}

	/**
	 * Creates the user login for rendering in the Accounts login view
	 */
	public static void login() {
		render();
	}

	/**
	 * Creates the user logout by diverting the User to the Welcome landing page
	 */
	public static void logout() {
		session.clear();
		Welcome.index();
	}

	/**
	 * Affirms the current user by searching the firstName of User for the
	 * current session
	 * 
	 * @return the currently logged-in User
	 **/
	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate with " + email + ":" + password);

		User user = User.findByEmail(email);
		if ((user != null) && (user.checkPassword(password) == true)) 
			// refers to User model
		{
			Logger.info("Authentication successful");
			session.put("logged_in_userid", user.id);
			Welcome.index();
		} else {
			Logger.info("Authentication failed");
			login();
		}
	}

	public static User getCurrentUser() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			return null;
		}
		User logged_in_user = User.findById(Long.parseLong(userId)); 
		// refers to User model
		Logger.info("Logged in user is " + logged_in_user.firstName);
		return logged_in_user;
	}
}